﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSwitch : MonoBehaviour
{
    public GameObject cCamera;
    public GameObject posKoko;
    public GameObject posMon;
    public GameObject posNPC;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    #region Switch
    public void switchKoko()
    {
        cCamera.transform.position = posKoko.transform.position;
    }
    public void switchMon()
    {
        cCamera.transform.position = posMon.transform.position;
    }
    public void switchNPC()
    {
        cCamera.transform.position = posNPC.transform.position;
    }
    #endregion
}
