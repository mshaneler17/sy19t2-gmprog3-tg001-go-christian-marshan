﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCAnimationControl : MonoBehaviour
{
    #region Attributes

    private Animator animator;

    private const string IDLE_ANIMATION_BOOL = "isIdle";
    private const string TALK_ANIMATION_BOOL = "isTalk";

    #endregion
    #region Animate Functions

    public void AnimateIdle()
    {
        Animate(IDLE_ANIMATION_BOOL);
    }
    public void AnimateTalk()
    {
        Animate(TALK_ANIMATION_BOOL);
    }

    #endregion
    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    private void Animate(string boolName)
    {
        DisableOtherAnimations(animator, boolName);
        animator.SetBool(boolName, true);
    }

    // Update is called once per frame
    private void DisableOtherAnimations(Animator animator, string animation)
    {
        foreach (AnimatorControllerParameter parameter in animator.parameters)
        {
            if (parameter.name != animation)
            {
                animator.SetBool(parameter.name, false);
            }
        }
    }
}
