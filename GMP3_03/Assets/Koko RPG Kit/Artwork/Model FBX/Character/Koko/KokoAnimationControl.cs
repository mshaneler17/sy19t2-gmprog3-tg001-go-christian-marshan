﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KokoAnimationControl : MonoBehaviour
{
    #region Attributes

    private Animator animator;

    private const string IDLE_ANIMATION_BOOL = "isIdle";
    private const string DRAWBLADE_ANIMATION_BOOL = "isDrawBlade";
    private const string SKILL_ANIMATION_BOOL = "isSkill";
    private const string ATTACK_ANIMATION_BOOL = "isAttack";
    private const string ATTACKSTANDBY_ANIMATION_BOOL = "isAttackStandby";
    private const string DEAD_ANIMATION_BOOL = "isDead";
    private const string DAMAGE_ANIMATION_BOOL = "isDamage";
    private const string COMBO_ANIMATION_BOOL = "isCombo";
    private const string RUNNO_ANIMATION_BOOL = "isRunNo";
    private const string RUN_ANIMATION_BOOL = "isRun";
    private const string PUTBLADE_ANIMATION_BOOL = "isPutBlade";

    #endregion

    #region Animate Functions

    public void AnimateIdle()
    {
        Animate(IDLE_ANIMATION_BOOL);
    }
    public void AnimateDrawBlade()
    {
        Animate(DRAWBLADE_ANIMATION_BOOL);
    }
    public void AnimateSkill()
    {
        Animate(SKILL_ANIMATION_BOOL);
    }
    public void AnimateAttack()
    {
        Animate(ATTACK_ANIMATION_BOOL);
    }
    public void AnimateAttackStandby()
    {
        Animate(ATTACKSTANDBY_ANIMATION_BOOL);
    }
    public void AnimateDead()
    {
        Animate(DEAD_ANIMATION_BOOL);
    }
    public void AnimateDamage()
    {
        Animate(DAMAGE_ANIMATION_BOOL);
    }
    public void AnimateCombo()
    {
        Animate(COMBO_ANIMATION_BOOL);
    }
    public void AnimateRunNo()
    {
        Animate(RUNNO_ANIMATION_BOOL);
    }
    public void AnimateRun()
    {
        Animate(RUN_ANIMATION_BOOL);
    }
    public void AnimatePutBlade()
    {
        Animate(PUTBLADE_ANIMATION_BOOL);
    }

    #endregion
    // Start is called before the first frame update
    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    private void Animate(string boolName)
    {
        DisableOtherAnimations(animator, boolName);
        animator.SetBool(boolName, true);
    }

    // Update is called once per frame
    private void DisableOtherAnimations(Animator animator, string animation)
    {
        foreach(AnimatorControllerParameter parameter in animator.parameters)
        {
            if(parameter.name != animation)
            {
                animator.SetBool(parameter.name, false);
            }
        }
    }
}
