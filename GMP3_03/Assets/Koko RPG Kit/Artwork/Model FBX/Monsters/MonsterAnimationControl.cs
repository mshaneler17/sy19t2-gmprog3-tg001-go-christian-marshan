﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterAnimationControl : MonoBehaviour
{
    #region Attributes

    private Animator animator;

    private const string IDLE_ANIMATION_BOOL = "isIdle";
    private const string ATTACK_ANIMATION_BOOL = "isAttack";
    private const string ATTACKNO_ANIMATION_BOOL = "isAttackNo";
    private const string DEAD_ANIMATION_BOOL = "isDead";
    private const string DAMAGE_ANIMATION_BOOL = "isDamage";
    private const string RUN_ANIMATION_BOOL = "isRun";
    private const string WALK_ANIMATION_BOOL = "isWalk";

    #endregion

    #region Animate Functions

    public void AnimateIdle()
    {
        Animate(IDLE_ANIMATION_BOOL);
    }
    public void AnimateAttack()
    {
        Animate(ATTACK_ANIMATION_BOOL);
    }
    public void AnimateAttackNo()
    {
        Animate(ATTACKNO_ANIMATION_BOOL);
    }
    public void AnimateDead()
    {
        Animate(DEAD_ANIMATION_BOOL);
    }
    public void AnimateDamage()
    {
        Animate(DAMAGE_ANIMATION_BOOL);
    }
    public void AnimateRun()
    {
        Animate(RUN_ANIMATION_BOOL);
    }
    public void AnimateWalk()
    {
        Animate(WALK_ANIMATION_BOOL);
    }

    #endregion
    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    private void Animate(string boolName)
    {
        DisableOtherAnimations(animator, boolName);
        animator.SetBool(boolName, true);
    }

    // Update is called once per frame
    private void DisableOtherAnimations(Animator animator, string animation)
    {
        foreach (AnimatorControllerParameter parameter in animator.parameters)
        {
            if (parameter.name != animation)
            {
                animator.SetBool(parameter.name, false);
            }
        }
    }
}
