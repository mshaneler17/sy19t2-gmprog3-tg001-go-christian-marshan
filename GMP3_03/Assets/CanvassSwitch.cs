﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvassSwitch : MonoBehaviour
{
    public GameObject UIKoko;
    public GameObject UIMon;
    public GameObject UINPC;
    // Start is called before the first frame update
    void Start()
    {
        UIKoko.SetActive(false);
        UIMon.SetActive(false);
        UINPC.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    #region UI Switch

    public void KokoUISwitch()
    {
        UIKoko.SetActive(true);
        UIMon.SetActive(false);
        UINPC.SetActive(false);  
    }
    public void MonUISwitch()
    {
        UIKoko.SetActive(false);
        UIMon.SetActive(true);
        UINPC.SetActive(false);
    }
    public void NPCUISwitch()
    {
        UIKoko.SetActive(false);
        UIMon.SetActive(false);
        UINPC.SetActive(true);
    }

    #endregion
}
